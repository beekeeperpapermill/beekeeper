import * as express from "express";

import * as mongodb from "mongodb";
import { Customers } from "./customers/customers";
const app = express();
const port = 8080; // default port to listen
const mongo = mongodb.MongoClient;

const path = "mongodb://kea:kea1234@5.186.58.7:27017/Beekeeper";

const prefix = '/api'

// define a route handler for the default home page
app.get(prefix + "/companies/list", async (req, res) => {
    mongo.connect(path, async (err, client) => {
        if (err) {
            throw err;
        }
        const db = client.db();

        const collection = db.collection("companies");

        collection.find({}, { projection: { _id: 0, company_id: 1, company_name: 1 } }).toArray().then((callThis) => {
            res.json(callThis);
        });
    });
});

app.get(prefix + "/companies/get/:id", (req, res) => {

    const id = +req.params.id;

    mongo.connect(path, async (err, client) => {
        if (err) {
            throw err;
        }

        const db = client.db();

        const collection = db.collection("companies");

        collection.findOne({ company_id: id }).then((callThis) => {
            res.json(callThis);
        });

    });

});

// Get a list of all customer names and ID
app.get(prefix + '/customers/names', (_req, res) => Customers.getNameIdList(_req, res));

// Get income & turnover for a customer
app.get(prefix + '/customers/bardata/income', (_req, res) => res.send(Customers.getBarDataIncome(_req, res)));
app.get(prefix + '/customers/bardata/turnover', (_req, res) => res.send(Customers.getBarDataTurnover(_req, res)));
app.get(prefix + '/customers/trades/:id', (req, res) => Customers.getTradesList(req.params.id, res));
app.get(prefix + '/customers/keyfigures/:id', (req, res) => Customers.getIncomeTurnover(req.params.id, res));
app.get(prefix + '/customers/related/:id', (req, res) => Customers.getRelatedCompanies(req.params.id, res));
app.get(prefix + '/customers/users/:id', (req, res) => Customers.getUsers(req.params.id, res));

// Start the Express server
app.listen(port, () => {
    console.log(`server started at http://localhost:${port}`);
});
