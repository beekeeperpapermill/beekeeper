import * as mongodb from "mongodb";

const mongo = mongodb.MongoClient;

const path = "mongodb://kea:kea1234@5.186.58.7:27017/Beekeeper";

export class Customers {

    static getNameIdList(_req, res) {
        mongo.connect(path, async (err, client) => {
            if (err) {
                throw err;
            }
            const db = client.db();
            const collection = db.collection("companies");

            collection.find({}, { projection: { _id: 0, company_id: 1, company_name: 1 } }).toArray().then((list) => {
                res.json(list);
                client.close();
            });
        });
    }

    static getTradesList(id, res) {
        mongo.connect(path, async (err, client) => {
            if (err) {
                throw err;
            }
            const db = client.db();
            const collection = db.collection("trades");

            collection.find({company_id: + id}).toArray().then((list) => {
                res.json({'data': list});
                client.close();
            });
        });
    }

    static getIncomeTurnover(id, res) {

        mongo.connect(path, async (err, client) => {
            if (err) {
                throw err;
            }
            const db = client.db();
            const collection = db.collection("companies");

            collection.find({ company_id: +id }).toArray().then((list) => {
                let incomeAccumulated = 0;
                let turnoverAccumulated = 0;
                let incomeMonth = list[0].income[list[0].income.length - 1].income;
                let turnoverMonth = list[0].turnover[list[0].turnover.length - 1].income;
                let company_name = list[0].company_name;

                for (let i = 0; i < list[0].income.length; i++) {
                    incomeAccumulated += list[0].income[i].income;
                }
                for (let i = 0; i < list[0].turnover.length; i++) {
                    turnoverAccumulated += list[0].turnover[i].income;
                }

                res.json({ "data": { incomeAccumulated, turnoverAccumulated, incomeMonth, turnoverMonth, company_name } });
                client.close();
            });
        });
    }

    static getUsers(id, res) {
         mongo.connect(path, async (err, client) => {
            if (err) {
                throw err;
            }
            const db = client.db();
            const collection = db.collection("companies");

            collection.find({ company_id: +id }, {projection: {_id: 0} }).toArray().then((list) => {
  

                res.json({ "data": {  } });
                client.close();
            });
        });
    }

    static getRelatedCompanies(id, res) {
        mongo.connect(path, async (err, client) => {
            if (err) {
                throw err;
            }
            const db = client.db();
            let related_companies = [];

            const collection = db.collection("companies");

            collection.find({ company_id: +id }).toArray().then((list) => {

                related_companies = list[0].related_companies_id.toString().split(',').map(Number);

                collection.find({ company_id: { $in: related_companies } }).toArray().then((list) => {
                    let daughter_companies = []
                    for (let i = 0; i < list.length; i++) {
                        daughter_companies.push({ 'company_id': list[i].company_id, 'company_name': list[i].company_name })
                    }
                    res.json({ 'data': { daughter_companies } });
                    client.close();
                });
            });
        });
    }

    static getBarDataIncome(_req, _res) {
        let barDataIncome =
            [
                {
                    category: "January",
                    objects: [
                        {
                            value: Math.round(Math.random() * 100000),
                            label: "2018"
                        },
                        {
                            value: Math.round(Math.random() * 100000),
                            label: "2019"
                        }
                    ]
                },
                {
                    category: "February",
                    objects: [
                        {
                            value: Math.round(Math.random() * 100000),
                            label: "2018"
                        },
                        {
                            value: Math.round(Math.random() * 100000),
                            label: "2019"
                        }
                    ]
                },
                {
                    category: "March",
                    objects: [
                        {
                            value: Math.round(Math.random() * 100000),
                            label: "2018"
                        },
                        {
                            value: Math.round(Math.random() * 100000),
                            label: "2019"
                        }
                    ]
                },
                {
                    category: "April",
                    objects: [
                        {
                            value: Math.round(Math.random() * 100000),
                            label: "2018"
                        },
                        {
                            value: Math.round(Math.random() * 100000),
                            label: "2019"
                        }
                    ]
                },
                {
                    category: "May",
                    objects: [
                        {
                            value: Math.round(Math.random() * 100000),
                            label: "2018"
                        },
                        {
                            value: Math.round(Math.random() * 100000),
                            label: "2019"
                        }
                    ]
                },
                {
                    category: "June",
                    objects: [
                        {
                            value: Math.round(Math.random() * 100000),
                            label: "2018"
                        },
                        {
                            value: Math.round(Math.random() * 100000),
                            label: "2019"
                        }
                    ]
                },
                {
                    category: "July",
                    objects: [
                        {
                            value: Math.round(Math.random() * 100000),
                            label: "2018"
                        },
                        {
                            value: Math.round(Math.random() * 100000),
                            label: "2019"
                        }
                    ]
                },
                {
                    category: "August",
                    objects: [
                        {
                            value: Math.round(Math.random() * 100000),
                            label: "2018"
                        },
                        {
                            value: Math.round(Math.random() * 100000),
                            label: "2019"
                        }
                    ]
                },
                {
                    category: "September",
                    objects: [
                        {
                            value: Math.round(Math.random() * 100000),
                            label: "2018"
                        },
                        {
                            value: Math.round(Math.random() * 100000),
                            label: "2019"
                        }
                    ]
                },
                {
                    category: "October",
                    objects: [
                        {
                            value: Math.round(Math.random() * 100000),
                            label: "2018"
                        },
                        {
                            value: Math.round(Math.random() * 100000),
                            label: "2019"
                        }
                    ]
                },
                {
                    category: "November",
                    objects: [
                        {
                            value: Math.round(Math.random() * 100000),
                            label: "2018"
                        },
                        {
                            value: 0,
                            label: "2019"
                        }
                    ]
                },
                {
                    category: "December",
                    objects: [
                        {
                            value: Math.round(Math.random() * 100000),
                            label: "2018"
                        },
                        {
                            value: 0,
                            label: "2019"
                        }
                    ]
                }
            ];
        return {
            "data": barDataIncome
        }
    }

    static getBarDataTurnover(_req, _res) {
        let barDataTurnover =
            [
                {
                    category: "January",
                    objects: [
                        {
                            value: Math.round(Math.random() * 1000000),
                            label: "2018"
                        },
                        {
                            value: Math.round(Math.random() * 1000000),
                            label: "2019"
                        }
                    ]
                },
                {
                    category: "February",
                    objects: [
                        {
                            value: Math.round(Math.random() * 1000000),
                            label: "2018"
                        },
                        {
                            value: Math.round(Math.random() * 1000000),
                            label: "2019"
                        }
                    ]
                },
                {
                    category: "March",
                    objects: [
                        {
                            value: Math.round(Math.random() * 1000000),
                            label: "2018"
                        },
                        {
                            value: Math.round(Math.random() * 1000000),
                            label: "2019"
                        }
                    ]
                },
                {
                    category: "April",
                    objects: [
                        {
                            value: Math.round(Math.random() * 1000000),
                            label: "2018"
                        },
                        {
                            value: Math.round(Math.random() * 1000000),
                            label: "2019"
                        }
                    ]
                },
                {
                    category: "May",
                    objects: [
                        {
                            value: Math.round(Math.random() * 1000000),
                            label: "2018"
                        },
                        {
                            value: Math.round(Math.random() * 1000000),
                            label: "2019"
                        }
                    ]
                },
                {
                    category: "June",
                    objects: [
                        {
                            value: Math.round(Math.random() * 1000000),
                            label: "2018"
                        },
                        {
                            value: Math.round(Math.random() * 1000000),
                            label: "2019"
                        }
                    ]
                },
                {
                    category: "July",
                    objects: [
                        {
                            value: Math.round(Math.random() * 1000000),
                            label: "2018"
                        },
                        {
                            value: Math.round(Math.random() * 1000000),
                            label: "2019"
                        }
                    ]
                },
                {
                    category: "August",
                    objects: [
                        {
                            value: Math.round(Math.random() * 1000000),
                            label: "2018"
                        },
                        {
                            value: Math.round(Math.random() * 1000000),
                            label: "2019"
                        }
                    ]
                },
                {
                    category: "September",
                    objects: [
                        {
                            value: Math.round(Math.random() * 1000000),
                            label: "2018"
                        },
                        {
                            value: Math.round(Math.random() * 1000000),
                            label: "2019"
                        }
                    ]
                },
                {
                    category: "October",
                    objects: [
                        {
                            value: Math.round(Math.random() * 1000000),
                            label: "2018"
                        },
                        {
                            value: Math.round(Math.random() * 1000000),
                            label: "2019"
                        }
                    ]
                },
                {
                    category: "November",
                    objects: [
                        {
                            value: Math.round(Math.random() * 1000000),
                            label: "2018"
                        },
                        {
                            value: 0,
                            label: "2019"
                        }
                    ]
                },
                {
                    category: "December",
                    objects: [
                        {
                            value: Math.round(Math.random() * 1000000),
                            label: "2018"
                        },
                        {
                            value: 0,
                            label: "2019"
                        }
                    ]
                }
            ];
        return {
            "data": barDataTurnover
        }
    }

}