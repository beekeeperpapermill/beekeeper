"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mongodb = __importStar(require("mongodb"));
const app = express_1.default();
const port = 8080; // default port to listen
const mongo = mongodb.MongoClient;
const path = "mongodb://kea:kea1234@5.186.58.7:27017/Beekeeper";
// define a route handler for the default home page
app.get("/companies/list", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    mongo.connect(path, (err, client) => __awaiter(void 0, void 0, void 0, function* () {
        if (err) {
            throw err;
        }
        const db = client.db();
        const collection = db.collection("companies");
        collection.find({}, { projection: { _id: 0, company_id: 1, company_name: 1 } }).toArray().then((callThis) => {
            res.json(callThis);
        });
    }));
}));
app.get("/companies/get/:id", (req, res) => {
    const id = +req.params.id;
    mongo.connect(path, (err, client) => __awaiter(void 0, void 0, void 0, function* () {
        if (err) {
            throw err;
        }
        const db = client.db();
        const collection = db.collection("companies");
        collection.findOne({ company_id: id }).then((callThis) => {
            res.json(callThis);
        });
    }));
});
// start the Express server
app.listen(port, () => {
    console.log(`server started at http://localhost:${port}`);
});
//# sourceMappingURL=index.js.map