import { Component, OnInit, ElementRef, ViewChild, Input, OnDestroy, OnChanges, NgZone } from '@angular/core';
import { Subject, fromEvent } from 'rxjs';
import * as d3 from 'd3';
import { debounceTime, takeUntil } from 'rxjs/operators';
import moment = require('moment');

interface Margin {
  top: number;
  right: number;
  bottom: number;
  left: number;
}

interface Dimensions {
  width: number;
  height: number;
}

interface DataObject {
  value: number;
  label: string;
}

interface Data {
  category: string;
  objects: DataObject[];
}

@Component({
  selector: 'bkp-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})

export class BarChartComponent implements OnInit, OnDestroy, OnChanges {
  @ViewChild('chart', { static: true }) chartElement: ElementRef;
  @Input() data: Data[];
  @Input() margin: Margin = { top: 20, right: 0, bottom: 80, left: 50 };

  currentYear = moment().year();
  legendList = [this.currentYear, this.currentYear - 1];
  barData = [];
  destroyed = new Subject();

  chart_selection: d3.Selection<d3.BaseType, {}, SVGElement, any>;
  svg: d3.Selection<SVGElement, {}, HTMLElement, any>;
  chart: d3.Selection<SVGElement, {}, HTMLElement, any>;
  legend: d3.Selection<SVGElement, {}, HTMLElement, any>;

  // Scales
  yScale: d3.ScaleLinear<number, number>;
  xScale: d3.ScaleBand<string>;
  legendScale: d3.ScaleBand<string>;

  // Axis
  xAxis: d3.Selection<SVGElement, {}, HTMLElement, any>;
  yAxis: d3.Selection<SVGElement, {}, HTMLElement, any>;
  xAxisGroup: d3.Selection<SVGElement, {}, HTMLElement, any>;
  yAxisGroup: d3.Selection<SVGElement, {}, HTMLElement, any>;

  // Sizing
  canvasDim: Dimensions;
  chartDim: Dimensions;

  constructor(private zone: NgZone) { }

  ngOnInit() {
    // Draw chart
    this.drawChart();
    // Set color gradient
    // this.SetColorGradient();
    // Update chart
    this.updateChart();

    // Subscribe to window resize events
    // Run outside angular
    this.zone.runOutsideAngular(() =>
      fromEvent(window, 'resize')
        .pipe(
          debounceTime(100),
          takeUntil(this.destroyed)
        )
        .subscribe(() => {
          console.log("RESIZED NOW")
          this.updateChart();
        }));
  }

  ngOnChanges() {
    this.updateChart();
  }

  ngOnDestroy() {
    this.destroyed.next();
    this.destroyed.complete();
  }

  calculateDimensions() {

    this.canvasDim = {
      width: this.chartElement.nativeElement.clientWidth,
      height: this.chartElement.nativeElement.clientHeight
    }; 

    this.chartDim = {
      width: this.canvasDim.width - this.margin.right - this.margin.left,
      height: this.canvasDim.height - this.margin.top - this.margin.bottom
    };
  }

  drawChart() {
    // Append svg element
    this.svg = d3.select(this.chartElement.nativeElement)
      .append('svg');

    // Create axises
    this.xAxisGroup = this.svg.append('g').attr('class', 'x-axis');
    this.yAxisGroup = this.svg.append('g').attr('class', 'y-axis');

    // Create chart group, where the chart is going to be drawn
    this.chart = this.svg.append('g');
    this.legend = this.svg.append('g');
  }

  updateChart() {

    // ERROR HANDLING
    // Make sure that there is data before updating chart
    if (!this.data) { return; }
    // Make sure that an SVG has been drawn
    if (typeof this.svg === 'undefined') { return; }

    this.barData = this.data.concat.apply([], (this.data.map(x => x.objects.map(y => ({ category: x.category, ...y })))));
    this.calculateDimensions();

    this.svg
      .attr('width', this.canvasDim.width)
      .attr('height', this.canvasDim.height);

    // Scaling for x
    this.xScale = d3.scaleBand()
      .domain(this.barData.map(x => x.category))
      .range([0, this.chartDim.width])
      .padding(0.25);

    // Scaling for legend
    this.legendScale = d3.scaleBand()
      .domain(["2018", "2019"])
      .range([0, this.chartDim.width])
      .padding(0.25);

    // Scaling for y
    this.yScale = d3.scaleLinear()
      .domain(
        [d3.min(this.data, d => Math.min(d.objects[0].value, d.objects[1].value)) < 0 ?
          d3.min(this.data, d => Math.min(d.objects[0].value, d.objects[1].value)) :
          0,
        d3.max(this.data, d => Math.max(d.objects[0].value, d.objects[1].value) + Math.max(d.objects[0].value, d.objects[1].value) * .15)])
      .range([this.chartDim.height, 0]);

    // Axis and label orientation for x 
    this.xAxisGroup
      .attr('transform', `translate(${this.margin.left}, ${this.chartDim.height + this.margin.top})`)
      .call(d3.axisBottom(this.xScale))
      .selectAll('.tick')
      .selectAll('text')
      .style("shape-rendering", "crispEdges")
      .attr("font-size", "14px")
      .call(this.setLabelOrientation, this.xScale.bandwidth());

    // Axis for y
    this.yAxisGroup
      .attr('transform', `translate(${this.margin.left},${this.margin.top})`)
      .call(d3.axisLeft(this.yScale)
        .ticks(8)
        .tickSize(-this.chartDim.width)
        .tickFormat(d => {
          const s = String(d).charAt(0).toUpperCase() + String(d).slice(1);
          if (!isNaN(Number(d))) { // test that value is a number
            return this.formatNumber(Number(d));
          }
          return s;
        })
        .tickSizeOuter(0)
      )
      .selectAll("text")
      .style("shape-rendering", "crispEdges")

      // .attr("display", ((d, i) => (i % 2 === 1 && i > 14) ?
      //   "none" :
      //   "initial"
      // ))
      .attr("font-size", "12px")

    this.yAxisGroup
      .selectAll(".tick line")
      .style("shape-rendering", "crispEdges")
      .attr("opacity", "0.4");

    // Update chart scaling
    this.chart
      .attr('width', this.chartDim.width)
      .attr('height', this.chartDim.height)
      .attr('transform', `translate(${this.margin.left},${this.margin.top})`);

    // Append bars to chart
    const bars = this.chart
      .selectAll('rect')
      .data(this.barData);

    // Remove exit selection
    bars.exit().remove().transition();

    bars
      .transition()
      .delay((d, i) => i * 30)
      .duration(500)
      .attr('x', ((d, i) => (i % 2 == 0) ?
        this.xScale(d.category) :
        this.xScale(d.category) + this.xScale.bandwidth() / 4
      ))
      .attr('y', d => this.yScale(d.value))
      .attr('height', d => this.chartDim.height - this.yScale(d.value))
      .attr('width', this.xScale.bandwidth() / 2)
      .attr('fill', ((_d, i) => (i % 2 == 1) ?
        "#3399ff" :
        "#00005e"
      ));
      // .call(selection => this.barStyles(selection as any));

    bars
      .enter()
      .append('rect')
      .attr('height', d => this.chartDim.height - this.yScale(d.value))
      .attr('width', this.xScale.bandwidth() / 2)
      .attr('x', ((d, i) => (i % 2 == 0) ?
        this.xScale(d.category) :
        this.xScale(d.category) + this.xScale.bandwidth() / 4
      ))
      .attr('y', d => this.yScale(d.value))
      .attr('fill', ((_d, i) => (i % 2 == 1) ?
        "#3399ff" :
        "#00005e"
      ))
      .style("shape-rendering", "crispEdges")
      .attr('opacity', ((_d, i) => (i % 2 == 1) ?
        '1' :
        '0.84'
      ))
      // .call(selection => this.barStyles(selection as any))
      .append('title')
      .attr('value', d => "€ " + d.value)
      .text(d => "€ " + d.value);
    bars.selectAll('title').remove();
    bars
      .append('title')
      .attr('value', d => "€ " + d.value)
      .text(d => "€ " + d.value);

    this.createLegends();

  }

  barStyles(selection: d3.Selection<d3.BaseType, Data, SVGElement, any>) {
    const curvature = Math.round(this.xScale.bandwidth() / 22);
    selection
      .attr('rx', curvature)
      .attr('ry', curvature)
  }

  setLabelOrientation(selection: any, width: number) {
    /**
     * Formats axis labels to be horizental or vertical
     *
     * @example
     * A text selection with a given orientation will
     * have its orientation changed based on the longest text
     * or the width of the bars in the chart.
     *
     * @param selection Selection to be modified
     * @param width Width of bar individual bar chart bar
     * @returns The selection
     */

    let flip = false;
    selection._groups[0].forEach(element => {
      if (d3.select(element).text().length > 6) {
        flip = true;
        return;
      }
    });
    if (width < 30 || flip) {
      return selection
        .attr('transform', 'rotate(90)')
        .attr('y', 0)
        .attr('x', 9)
        .attr('dy', '.35em')
        .style('text-anchor', 'start');
    } else {
      return selection
        .attr('transform', 'rotate(0)')
        .attr('y', 9)
        .attr('x', 0)
        .attr('dy', '.71em')
        .style('text-anchor', 'middle');
    }
  }

  formatNumber(n: number): string {
    /**
     * Perform number formatting for large numbers
     * @param n Number to be formatted
     * @returns A formatted string
    */
    if (n > Math.pow(10, 9) - 1) {
      return this.createNumberWithSuffix(n, 9, 'B')
    } else if (n > Math.pow(10, 6) - 1) {
      return this.createNumberWithSuffix(n, 6, 'M')
    } else if (n > Math.pow(10, 3) - 1) {
      return this.createNumberWithSuffix(n, 3, 'K')
    } else {
      return String(n);
    }
  }

  createNumberWithSuffix(number, power, suffix) {
    if (number % Math.pow(10, power) === 0) {
      return Math.round(number / Math.pow(10, power)) + suffix;
    } else {
      return (number / Math.pow(10, power)).toFixed(1) + suffix;
    }
  }

  // SetColorGradient() {
  //   const defs = this.svg.append('defs');
  //   const bgGradientNew = defs
  //     .append('linearGradient')
  //     .attr('id', 'bg-gradient-new')
  //     .attr('gradientTransform', 'rotate(90)');
  //   bgGradientNew
  //     .append('stop')
  //     .attr('stop-color', '#3399ff')
  //     .attr('offset', '0%');
  //   bgGradientNew
  //     .append('stop')
  //     .attr('stop-color', '#0000ff')
  //     .attr('offset', '100%');
  //   const bgGradientOld = defs
  //     .append('linearGradient')
  //     .attr('id', 'bg-gradient-old')
  //     .attr('gradientTransform', 'rotate(90)');
  //   bgGradientOld
  //     .append('stop')
  //     .attr('stop-color', '#00005E')
  //     .attr('offset', '0%');
  //   bgGradientOld
  //     .append('stop')
  //     .attr('stop-color', '#0000A0')
  //     .attr('offset', '100%');
  // }

  createLegends() {
    this.legend
      .attr("class", "legend")
      .attr("transform", `translate(${Math.floor(this.canvasDim.width - this.margin.left * 2.3)}, 0)`);

    this.legend.selectAll("rect")
      .data(['2018', '2019'])
      .enter()
      .append("rect")
      .attr('x', ((d, i) => i * 70))
      .attr("width", 13)
      .attr("height", 13)
      .attr('fill', ((_d, i) => (i % 2 == 1) ?
        "#3399ff" :
        "#00005e"
      ));
      // .call(selection => this.barStyles(selection as any));

    this.legend.selectAll("text")
      .data(['2018', '2019'])
      .enter()
      .append("text")
      .attr('x', ((d, i) => i * 70 + 16))
      .attr("y", 7)
      .attr("dy", ".35em")
      .attr('font-size', '12px')
      .style("text-anchor", "start")
      .text(d => d);
  }
}
