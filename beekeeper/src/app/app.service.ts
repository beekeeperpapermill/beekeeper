import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) { }

  
    getCompaniesNamesList(): any{
      const url = '/api/customers/names';
      return this.http.get(url).pipe(
        map(res => res)
      );
    }

  
}
