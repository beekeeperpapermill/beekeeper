import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { BarData, BarDataResponse, KeyfiguresResponse, Keyfigures, RelatedCompanies, RelatedCompaniesResponse, User, UserResponse, TradesResponse, Trades } from '../entity';
import { Observable } from 'rxjs';
import {Cacheable, GlobalCacheConfig} from 'ngx-cacheable';
import { SessionDomStrategy } from '../custom-cache-strategies/SessionDomStrategy';
GlobalCacheConfig.storageStrategy = SessionDomStrategy;

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  constructor(private http: HttpClient) { }

  @Cacheable({
    maxAge: 300000,
    slidingExpiration: true
  })
  getBarDataIncome(): Observable<BarData[]> {
    const url = '/api/customers/bardata/income';
    return this.http.get<BarDataResponse>(url).pipe(
      map(res => res.data)
    );
  }

  @Cacheable({
    maxAge: 300000,
    slidingExpiration: true
  })
  getBarDataTurnover(): Observable<BarData[]> {
    const url = '/api/customers/bardata/turnover';
    return this.http.get<BarDataResponse>(url).pipe(
      map(res => res.data)
    );
  }

  getCustomerKeyfigures(id : number) : Observable<Keyfigures> {
    const url = '/api/customers/keyfigures/' + id;
    return this.http.get<KeyfiguresResponse>(url).pipe(
      map(res => res.data)
    );
  }

  getRelatedCustomers(id: number) : Observable<RelatedCompanies> {
    const url = '/api/customers/related/' + id;
    return this.http.get<RelatedCompaniesResponse>(url).pipe(
      map(res => res.data)
    );
  }

  getUsers(id: number) : Observable<User>{
    const url = '/api/customers/users/' + id;
    return this.http.get<UserResponse>(url).pipe(
      map(res => res.data)
    );
  }

  getTrades(id: number) : Observable<Trades>{
    const url = '/api/customers/trades/' + id;
    return this.http.get<TradesResponse>(url).pipe(
      map(res => res.data)
    );
  }

}
