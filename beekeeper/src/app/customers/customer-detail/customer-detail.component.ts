import { Component, OnInit, OnChanges } from '@angular/core';
import { CustomersService } from '../customers.service';
import { Keyfigures } from 'src/app/entity';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.scss']
})
export class CustomerDetailComponent implements OnInit {

  public barData$;
  public keyfigures$;
  public relatedCompanies$;
  public trades$;
  public rights = ['FX', 'FAKEnews', 'niceData', 'testt'];
  public companies = ['Banjos Likørstue', 'Nåja APS', 'URE SMYKKER', 'Belle Delphines'];

  public isIncome = true;

  constructor(private customersService: CustomersService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        const id = +params['id'];
        this.fetchData(id);
      }
    );
  }

  fetchData(id: number) {
    this.relatedCompanies$ = this.customersService.getRelatedCustomers(id);
    this.barData$ = this.customersService.getBarDataIncome();
    this.keyfigures$ = this.customersService.getCustomerKeyfigures(id);
    this.trades$ = this.customersService.getTrades(id);
  }

  setBarData(isIncome: boolean) {
    if (isIncome) {
      this.barData$ = this.customersService.getBarDataIncome();
    } else {
      this.barData$ = this.customersService.getBarDataTurnover();
    }
  }

  changeUrl(id){
    this.router.navigate([`/customer-detail/${id}`]);
  }

  public formatNumber(n: number): string {
    /**
     * Perform number formatting for large numbers
     * @param n Number to be formatted
     * @returns A formatted string
    */
    if (n > Math.pow(10, 9) - 1) {
      return this.createNumberWithSuffix(n, 9, 'B')
    } else if (n > Math.pow(10, 6) - 1) {
      return this.createNumberWithSuffix(n, 6, 'M')
    } else if (n > Math.pow(10, 3) - 1) {
      return this.createNumberWithSuffix(n, 3, 'K')
    } else {
      return String(n);
    }
  }

  public createNumberWithSuffix(number, power, suffix) {
    if (number % Math.pow(10, power) === 0) {
      return Math.round(number / Math.pow(10, power)) + suffix;
    } else {
      return (number / Math.pow(10, power)).toFixed(1) + suffix;
    }
  }

}

