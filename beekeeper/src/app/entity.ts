export interface Customer {
    company_id: number;
    accesses: string[];
    company_name: string;
    shortname: string;
    created_on: Date;
    customer_number: number;
    currencies: string[];
    trade_activity: any;
    income: MonthlyStats[];
    turnover: MonthlyStats[];
    related_companies: number[];
}

export interface CustomerList {
    company_id: number;
    company_name: string;
}

export interface MonthlyStats {
    year: number;
    month: string;
    amount: number;
}

export interface Trade {
    company_id: number;
    traded_on: Date;
    amount_original: number;
    amount_received: number;
    currency_original: string;
    currency_received: string;
    type: string;
    rate: number;
}

export interface BarData {
    category: string;
    objects: BarChartData[];
}

export interface BarDataResponse {
    data: BarData[];
}

export interface BarChartData {
    value: number;
    label: string;
}

export interface Keyfigures {
    incomeAccumulated: number;
    turnoverAccumulated: number;
    incomeMonth: number;
    turnoverMonth: number;
}

export interface KeyfiguresResponse {
    data: Keyfigures;
}

export interface RelatedCompanies {
    daughter_companies: [{
        company_id: number;
        company_name: string;
    }]
}
export interface RelatedCompaniesResponse {
    data: RelatedCompanies;
}

export interface User {
    name: string;
}

export interface UserResponse {
    data: User;
}

export interface Trades {

}

export interface TradesResponse {
    data: Trades[];
}
