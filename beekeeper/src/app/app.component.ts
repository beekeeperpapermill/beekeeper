import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';
import { Observable } from 'rxjs';
import { Customer } from './entity';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  customers: Observable<Customer[]>;

  constructor(private appService: AppService) { }

  ngOnInit() {
    this.customers = this.appService.getCompaniesNamesList();
    console.log(this.customers);
  }
}
