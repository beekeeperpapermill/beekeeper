import { Component, Input, OnInit } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, startWith, switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { CustomerList } from '../entity';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss'],
})
export class SearchBarComponent implements OnInit {
  @Input() customers: Observable<CustomerList[]>;
  public displayedCustomers: Observable<CustomerList[]>;
  value = '';
  private searchTerm: Observable<string> = of(" ");
  public keyUp = new Subject<KeyboardEvent>();

  constructor(private router: Router) { }

  ngOnInit() {

    this.searchTerm = this.keyUp.pipe(
      map(event => (event.target as HTMLTextAreaElement).value),
      debounceTime(200),
      distinctUntilChanged()
    );

    this.displayedCustomers = this.searchTerm.pipe(
      startWith(''),
      switchMap(searchTerm =>
        this.customers.pipe(
          map(customers => {
            console.log(customers);
            if (searchTerm === undefined || searchTerm.length < 1) {
              return customers;
            }
            console.log(customers.filter(customer => customer.company_name.toUpperCase().indexOf(searchTerm) != -1 ||
            customer.company_name.toLowerCase().indexOf(searchTerm) != -1))
            return customers.filter(customer => customer.company_name.toUpperCase().indexOf(searchTerm) != -1 ||
              customer.company_name.toLowerCase().indexOf(searchTerm) != -1)
          })
        ))
    )
  }

  keyPressed(event) {
    this.keyUp.next(event);
    console.log(event.target.value)
  }

  changeUrl(id){
    this.router.navigate([`/customer-detail/${id}`]);
  }

}
